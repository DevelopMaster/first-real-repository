import React from "react";
import FullWidthRow from "../components/FullWidthRow";
import FullScreenLogo from '../components/images/FullScreenLogo.jpg'

const About = () => {
    return(
        <>
        <FullWidthRow>
          <img src={FullScreenLogo} className="img-fluid" style={{maxHeight: "500px"}} alt={"Something went wrong."}/>
        </FullWidthRow>
        </>
    )
}

export default About