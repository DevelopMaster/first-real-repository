import GoogleMapReact from "google-map-react";
import { Icon } from '@iconify/react';
import Pin from "./Pin";

const AnyReactComponent = (lat: number, lng: number, text: string) => <div>{text}</div>;

const Map = () => {

    const location = {
        // TODO: change address
        address: '1600 Amphitheatre Parkway, Mountain View, california.',
        lat: 51.8578267161795,
        lng: 19.396445143084442,
      }
      return(
        <div id={"google-maps-wrapper"}>
        <GoogleMapReact
        bootstrapURLKeys={{key: "AIzaSyD_Sw0ifnwwGbslerwJWyoPw8beiu6QG6o"}}
        defaultCenter={location}
        defaultZoom={17}
        options={{zoomControl: false, minZoom: 14, maxZoom:18}}
        >
            <Pin lat={location.lat} lng={location.lng} text={"text"}/>
      </GoogleMapReact>
      </div>
      )
}

export default Map