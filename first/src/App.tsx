import React from 'react';
import './App.css';
import Karolina from "./Karolina";
import Jarek from "./Jarek";
import Daniel from "./Daniel";

function App() {
  return (
    <div className="App">
      <div className={"relative-top-position"}/>
      <div className={"container"}>
        {/* <div className={"row"}>
          <div className={"col"} style={{borderRight: "1px solid black"}}>
            <Karolina/>
          </div>
          <div className={"col"} style={{borderLeft: "1px solid blue"}}>
            <Jarek/>
          </div>
        </div> */}
        <div className={"row"} style={{borderTop: "1px solid red"}}>
            <Daniel/>
        </div>
      </div>
    </div>
  );
}

export default App;
