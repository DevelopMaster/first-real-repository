import { ReactElement } from "react"
import { JsxElement } from "typescript"

export interface ICompanyAddress {
    streetName: string,
    postalCode: string,
    city: string,
    streetNumber: string
}

export interface IRegistryData {
    name: string,
    representative: string,
    taxIdentityNumber: string,
    nationalEconomyRegistryNumber: string,
}

interface ITableStyleRow {
    firsImg: ReactElement,
    firstText: string | ICompanyAddress | IRegistryData,
    secImg?: ReactElement,
    secText?: string | ICompanyAddress | IRegistryData
}

const TableStyleRow = (props: ITableStyleRow) => {

function isInstanceOfCompanyAddress (object: any): object is ICompanyAddress {
    return 'streetName' in object;
}

const generateColumnData = (obj: string|IRegistryData|ICompanyAddress) => {
    if (typeof obj === "string") {
        return(obj);
    }
    else if (isInstanceOfCompanyAddress(obj)) {
        return (
            <>
            Ul. {obj.streetName + " " + obj.streetNumber}<br/>
            {obj.postalCode + " " + obj.city}
        </>
        )
    }
    else return (
        <>
        <strong>{obj.name}</strong><br/>
        {obj.representative}<br/>
        <strong>NIP: </strong> {obj.taxIdentityNumber}<br/>
        <strong>REGON: </strong> {obj.nationalEconomyRegistryNumber}
        </>
    )
}

const generateAddressText = (obj: ICompanyAddress) => {
    return (
        <>
            Ul. {obj.streetName + " " + obj.streetNumber}<br/>
            {obj.postalCode + " " + obj.city}
        </>
    )
}

return(
    <div style={{display:"flex", marginTop:30}}>
    <div className="col-2" style={{textAlign:"center", alignSelf:"center"}}>
        {props.firsImg}
    </div>
    <div className="col-4"style={{textAlign:"start", alignSelf:"center"}}>
        {generateColumnData(props.firstText)}
    </div>
    {props.secImg && <div className="col-2"style={{textAlign:"center", alignSelf:"center"}}>
        {props.secImg}
    </div>}
    {props.secText && <div className="col-4"style={{textAlign:"start", alignSelf:"center"}}>
        {generateColumnData(props.secText)}
    </div>}
</div>
)
}

export default TableStyleRow