import React, {ReactNode} from 'react';

interface IRowProps {
  children: ReactNode
}

const FullWidthRow = (props: IRowProps) => {
  return (
      <div className={"row"}>
        <div className={"col-12"}>
          {props.children}
        </div>
      </div>
  )
};

export default FullWidthRow