import React from "react";
import GoogleMapReact from "google-map-react";
import './Contact.css'
import { Icon } from "@iconify/react";
import TableStyleRow, { ICompanyAddress, IRegistryData } from "../components/TableStyleRow";
import { CITY, EMAIL, NAME, NATIONAL_ECONOMY_REGISTRY_NUMBER, PHONE, POSTAL_CODE, REPRESENTATIVE, STREET_NAME, STREET_NUMBER, TAX_IDENTITY_NUMBER } from "../shared/enums/Constants";

interface ICompanyData {
    email: string,
    phone: string,
    registryData: IRegistryData
    companyAddress: ICompanyAddress
}

const Contact = () => {

    const companyData: ICompanyData = {
        email: EMAIL,
        phone: PHONE,
        registryData: {
        name: NAME,
        representative: REPRESENTATIVE,
        taxIdentityNumber: TAX_IDENTITY_NUMBER,
        nationalEconomyRegistryNumber: NATIONAL_ECONOMY_REGISTRY_NUMBER,
    },
        companyAddress: {
            streetName: STREET_NAME,
            streetNumber: STREET_NUMBER,
            postalCode: POSTAL_CODE,
            city: CITY
        }
    }
    const fontSize = 40

    const phoneImg = <Icon fontSize={fontSize} icon="fa-solid:phone-square-alt" />
    const companyImg = <Icon fontSize={fontSize} icon="clarity:building-solid" />
    const addressImg = <Icon fontSize={fontSize} icon="entypo:address" />
    const emailImg = <Icon fontSize={fontSize} icon="ic:baseline-email" />

    return (
        <div style={{marginTop:"20px"}}>
            <TableStyleRow firsImg={companyImg} firstText={companyData.registryData} secImg={phoneImg} secText={companyData.phone}/>
            <TableStyleRow firsImg={addressImg} firstText={companyData.companyAddress} secImg={emailImg} secText={companyData.email}/>
        </div>
    )
}

export default Contact;