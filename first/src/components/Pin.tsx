import { Icon } from '@iconify/react';

interface IPinProps {
    lat: number,
    lng: number,
    text: string
}

const Pin = (props: IPinProps) =>{
return(
    <div>
        <Icon fontSize={40}icon="akar-icons:arrow-up-left" color="red" />
    </div>
)
}

export default Pin