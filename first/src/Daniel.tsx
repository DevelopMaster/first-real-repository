
import FullWidthRow from "./components/FullWidthRow";
import CustomizedNavBar from "./components/CustomizedNavBar";
import { Map, GoogleApiWrapper } from 'google-maps-react';
import GoogleMapReact from 'google-map-react';
import { ReactElement, useState } from 'react';
import { JsxElement } from 'typescript';
import Contact from './pages/Contact';
import { Views } from './shared/enums/Views';
import About from './pages/About';
import './Daniel.css';

const Daniel = () => {

  const VIEWS = {
    [Views.CONTACT_VIEW]: <Contact/>,
    [Views.ABOUT_US]: <About/>
  }

  const [currentView, setCurrentView] = useState<ReactElement>(VIEWS[Views.ABOUT_US])
  const [showMap, setShowMap] = useState<boolean>(false);

  const moveToView = (value: Views) => {
    setCurrentView(VIEWS[value])
    if(value === Views.CONTACT_VIEW){
      setShowMap(true);
    }else{
      setShowMap(false);
    }
  }

  return (
      <div id={"page-wrapper"}>
        <CustomizedNavBar moveToView={moveToView} showMap={showMap}/>
        {currentView}
      </div>
  )

};

export default Daniel;