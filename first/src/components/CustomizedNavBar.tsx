import React, { Dispatch, SetStateAction } from 'react'
import { JsxElement } from 'typescript';
import { Views } from '../shared/enums/Views';
import FullWidthRow from "./FullWidthRow";
import FullScreenImg from "./images/FullScreenImg.png";
import Map from './Map'

interface ICustomizedNavBarProps {
  moveToView: (value: Views) => void;
  showMap: boolean;
}

const CustomizedNavBar = (props: ICustomizedNavBarProps) => {
  console.log(props.showMap)
  return (
    <>
      <nav className="navbar fixed-top navbar-expand-sm navbar-light bg-light">

        <div className={"container"}>

            <div className="container-fluid" style={{display:"flex", flexDirection: "row", verticalAlign: "center"}}>
              <div className=" navbar-collapse" id="navbarNav">
              <ul className="navbar-nav mr-auto">
                  <img src={FullScreenImg} className="img-fluid"
                       alt={"Something went wrong."} id={"image-hover"}/>
                <li className="nav-item" onClick={()=>props.moveToView(Views.ABOUT_US)}>
                  <p className={"nav-link"}><strong>O nas</strong></p>
                </li>
                <li className="nav-item" onClick={()=>props.moveToView(Views.CONTACT_VIEW)}>
                  <p className={"nav-link"}><strong>Kontakt</strong></p>
                </li>
              </ul>
              </div>
            </div>
        </div>
      </nav>
      {props.showMap && <Map/>}
      </>
  )
};

export default CustomizedNavBar